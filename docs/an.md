# Peoples Audio Wave Gen

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:1 -->

1. [Peoples Audio Wave Gen](#peoples-audio-wave-gen)
	1. [Introduction](#introduction)
	2. [Theory of Operation](#theory-of-operation)
	3. [Goals of Design](#goals-of-design)
	4. [Schematic](#schematic)

<!-- /TOC -->

## Introduction

The Gedanken PAWG-200 is a low-cost probe tool meant to inject frequencies
from DC to 40kHz at 50Ω, 75Ω, or 1MΩ (approximately, on the 1M setting) for
testing of audio equipment.

It features an adjustable voltage swing of 10Vpp, nominal. This can be
adjust to higher voltages, though this is not needed in most applications
and can cause damage.

## Theory of Operation

The generator uses nearly 100-year-old technology in the form of a PTC AGC, in
the form of an incandescent lamp. The specified part is ideal for the
schematic provided; however, adjustment of resistor `Rb`, and in more extreme
variations, adjustment of all values can accomodate other PTC devices.

It should be noted, however, that at this time there is no thermistor
availabble via retail channels that is appropriate.

To use, the user injects the signal wherever is convenient (if the equipment
is ready, the 75Ω setting and a pre-amp input, or a tape record input is
easiest, though in-circuit use will work fine), and the output is compared
with another instrument, such as a spectrum analyzer or oscilloscope.

The instrument has two separate outputs, and each is capable of driving the
input and output for comparison; ie, the instument can probe and be connected
to an oscilloscope, and two waveforms matched.

## Goals of Design

This generator is meant to be:
* Affordable: with or without purchase of the PCB, a user may already have the
  THT parts needed.
* Safe: The power section is DC, and has a virtual ground reference with no
  connection to mains power. It is meant to be battery-driven.
* Negligible THD: Optimum performance is ≤ 0.02 dBm, and can be depended on
  for ≤ 0.03 dBm performance within its bandwidth.
* Small: Despite using THT components to safely use high potentials for AC
  testing (30V), the PCB is ~10cm².

## Schematic

![Latest schematic](images/latest-schematic.png)
