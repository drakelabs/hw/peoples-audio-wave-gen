# Overall Design Documentation

One overall point that must be made is that this is in the Afghan lathe spirit
of tooling, while being a reliable and correct metrology tool, from DC to 40k.

There exist many affordable tools (though not necessarily FLOSS ones) that
provide a similar function in the RF (TinySA, etc.), but nothing much in the
audio range, and nothing analog that can potentially last a lifetime and not
suffer from aliasing of any kind, which is important for reburbishing and
experimentation in hifi, or just analog audio front ends of equipment.

## Notes On Current State of CAD documents

The idea is complete, and once the footprint for the 4 pots and the
attenuator, should be ready to send off to fab.

The stackup is two-sided, SS (obv), and less than 10cm square to hit the
sweet spot of deals at fabs. PCBWay seems like the winner, but such a simple
board could conceivably go anywhere, or even on copperclad. The tricky bit is
tuning impedance if going to bespoke route, so I heartily recommend the 5$ U.S
and patience on shipping to get a manufactured board. The mostly THT
components can definitely be added at your home base.

If you really wish to have the fab assemble, the only part that may be
difficult for them to get are the older PDIP opamps.

### TI TL07x/8x

They are in a socket because of some controversy about TI's respin of the
TL07x and TL08x opamps. The "H" series recommended since 2018 is not the same
chip, and does not have the "BiFET" front-end (JFET) that the original had,
and the noise measurement has been criticized for being measured in a
flattering light, _and_ there's now no real difference between 07x and 08x.

Googling will find conversations about this, it's not important enough for
documentation's sake; however, to end the section, the JFET-input opamps do
seem to perform very well at a very modest cost.

In any case, a socket is in the design.

## Power Rails

The voltage-splitter simplistic power is on a separate board, and meant to be
supplied with batteries (and so an already DC source.) It is on a separate
board as a better option is obviously doable, but just requires some number
crunching on cost. The goal is to make this instrument almost free.

## Resistance Varying

There's two variations of this oscillator: one that uses variable capacitors,
and one that uses variable resistors. This uses variable resistance due to
cost, and no real downside on reliability. The capacitance route is extremely
expensive these days, requiring truly awe-inspiring design and part sourcing
to accomplish for no real benefit.

It does mean, however, that we must use "range selection" and have a main
ganged pot and a vernier pot. Additionally, to hit the full range (which is
+/- 1dBm in my prototype), it will also require what was originally a stepped
attenuator.

The stepped attenuator route was soon nixed, unfortunately. It turns out that
switches are much more expensive when they're round than when they're inline,
so a make-before-break switch will be used for the same purpose, with a ladder
of three (6 in total per channel) resistors. With the pot and the switch, our
full modest bandwidth should be perfect and cheaply attainable.

## Capacitive coupling

The end of the oscillator stage is a 100nF film capacitor; the source and
properties are somewhat flexible. After this stage is the tuned impedance
routes and switch. It would be unfortunate if this design cannot internally
switch output impedance between 50Ω, 75Ω, and "high." Including this would
truly make it a ready-to-go tool to compare input and output of audio
circuits. It's almost a make-or-break feature, and I believe we can save
costs in other areas to make this possible and keep this affordable.

### Why the Impedance Switch?

One of the inspirations of the design of this, besides the affordability and
necessary utility, was the possibility of a battery-powered, safe (no ground
loops) and "integrateable" tool. The workflow, if not clear, could be
like this:

1. User connects 75Ω output in line-in or tape-in port, or in-circuit.
2. User compares output of DUT on a spectrum analyzer or oscilloscope.

## Enclosures

No official plan has been made for an enclosure. However, it may in some universe be possible to produce a durable case that could be purchased with the board, like some other small tools.

At the very least, we have the technology to make nice STLs, with which users can print or drill aluminum.

![Example enclosure](models/enclosure.png)

Gitlab can render interactive STLs:

[STL enclosure](https://gitlab.com/drakelabs/hw/peoples-audio-wave-gen/-/tree/main/docs/models/enclosure.stl)
