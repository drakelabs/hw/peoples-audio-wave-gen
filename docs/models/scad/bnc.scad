// Peoples Audio Wave Gen Panel

/*
BNC panel-mounted
Amphenol 31-10
units: mm
*/
$fn = 256;
eps = 0.01;

vals =
  [
    ["outer", 8.69],
    ["height", 20.7],
    ["inner", (8.69-0.25)]
  ];

v = function (i) vals[
  search([i], vals)[0]][1];

module BNC () {
  module Shell () {
    difference() {
      cylinder (d=v("outer"), h=v("height"), center=true);
      cylinder (d=v("inner"), h=v("height") + eps, center=true);
    }
  }

  Shell();
}

BNC();
