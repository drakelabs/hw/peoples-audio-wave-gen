// Peoples Audio Wave Gen Panel
// Front panel clearance based on Bourns PTD902
/*
w: width: x
h: height: z
d: depth: y

units: mm
*/
$fn = 64;

w = 105;
d = 105;
h = 25;

pcb_height = 1.6;
pcb_side = 99.06;
soldermask_color = [0.9, 0.9, 0.9, 1.0];

psu_w = 27.94;
psu_d = 71.12;

pot_d = 10.2;
panel_clear = 10;

material_color = [0.5, 0.5, 0.5, 1.0];
material_d = 2.5;

mil = 2.54;

module Enclosure () {
  module Container () {



    module FrontPanel () {
      margins = 10;
      bnc_dia = 8.69;
      bnc_h = 20.7;

      translate = function (d, pot_d, panel_clear) [
        0, 0, (d/2) + pot_d + panel_clear
      ];

      rotate = function () [90, 0, 0];

      module BNC () {
        cylinder (d=bnc_dia, h=bnc_h, center=true);
      }

      module ChannelControl () {
        margins = 10;
        bnc_dia = 8.69;
        bnc_h = 20.7;

        difference () {
          cube([
            w/2-margins,
            h-margins,
            1.0
          ], center=true);

          translate([-w/8, 0, 0])
            #BNC();

          translate([w/8, 0, 0])
            BNC();
        }
      }

    color(c=material_color)
    rotate(rotate())
    translate(translate(d, pot_d, panel_clear))
      cube ([w,h, material_d], center=true);

      translate([w/4, -(d-panel_clear-10*2)+1, (-h/2)+h/2])
      rotate([90,0,0])
        ChannelControl();

      translate([-(w/4), -(d-panel_clear-10*2)+1, (-h/2)+h/2])
      rotate([90,0,0])
        ChannelControl();
    }

    module BackPanel () {
      color(c=material_color)
      rotate ([90,0,0])
      translate([0,0,-pcb_side/2 - (panel_clear/2) + pcb_height]) // fudged a bit
        cube ([w,h, material_d], center=true);
    }

    module SidePanel (w,h) {
      color(c=material_color)
      cube([w + pot_d + panel_clear, h, material_d], center=true);
    }

    module MountingPad (d, hole_d) {
      difference () {
        color([0.94, 0.893, .7, 1.0])
          cylinder(h=pcb_height, d=d, center=true);
        cylinder(h=pcb_height, d=hole_d, center=true);
      }
    }

    // one FR4 stackup
    module PCB (w,d,h) {
      color(c=soldermask_color)
        cube ([w,d,h], center=true);

      for (i = [
        [(w+mil)/2-d, (d-mil)/2, 0],
        [(w-mil)/2, (d-mil)/2, 0],
        [(w+mil)/2-d, (-d+mil)/2, 0],
        [(w-mil)/2, (-d+mil)/2, 0]
      ]) {
        translate(i)
          MountingPad(h, 1.1);
      }
    }

    module PsuPCB (w,d,h) {
      cube ([w,d,h], center=true);
    }

    module PCBs () {
      translate([0,0, h/4])
        PCB(pcb_side,pcb_side,pcb_height);
      
      PCB(pcb_side,pcb_side,pcb_height);

      rotate([0,0,90])
      translate([(pcb_side - psu_d/2) - psu_w, 0, -(h/4)])
        PsuPCB(psu_w,psu_d,pcb_height);
    }

    module Sides () {
      rotate([90,0,90])
      translate([-(pot_d),0,-(pcb_side/2)-1.7])
        SidePanel(w,h);
      rotate([90,0,90])
      translate([-(pot_d),0,(pcb_side/2)+1.7])
        SidePanel(w,h);
    }

    PCBs();

    FrontPanel();
    BackPanel();
    Sides();
  }

  Container();
}

Enclosure();