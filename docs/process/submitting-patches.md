# Submitting patches

This will be a short document. More specific step-by-step procedures meant
for, e.g., specific environments, are welcome in separate files and
directories, should they grow large.

Patches submitted by any means are welcome: PR via whatever SaaS software,
email, carrier pigeon, etc.

## DCO and Sign-off

If not listed in MAINTAINERS.md, please sign off (see the `-s` flag in
`git-commit`), and if necessary, add a `cc:` if you wish to get particular
developer(s) attention. Those that are _in_ MAINTAINERS are encouraged to
sign, as well; but all with write access must be in MAINTAINERS.

# Please Reads

* `/LICENSE.md`
* `/CONTRIBUTING`

# FAQ

## Why AGPLv3.0 + DCO

For the AGPL, it's a variation on Greenspun's 10th rule: Every projects grows
to include, if not a full-blown SaaS, then something on the web. That will be
under the terms of the Affero license.

DCO seems to be the fairest of the options available legally. Certainly more
good-faith, in this case, than a CLA to the originator of the project. Your
work remains yours. Just remember to sign off, it's a lot of work to ask for
it on an otherwise perfect submission. Guides to configure Git per-project
for this abound.

# References

The process is lifted mostly from the Linux Kernel project; following its
guidelines will rarely differ from this project's requirements.

See also, if you're not already familiar with the GNU Public License, the
Developer's Certificate of Origin (DCO), and using Git in general:

## Linux Kernel Docs for Submitting Patches
* [submitting-patches.rst from v6.2-rc3](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/process/submitting-patches.rst?h=v6.2-rc3)

## Sign-off
* [Short explanation of DCO from the GCC project](https://gcc.gnu.org/dco.html)
* [Linux Foundation wiki page](https://wiki.linuxfoundation.org/dco)

