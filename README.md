# Simple Analog Audio Frequency Generator

Everyone's first project, but for real!

(Or many people's first project.)

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:1 -->

1. [Simple Analog Audio Frequency Generator](#simple-analog-audio-frequency-generator)
	1. [Hardware build and operation](#hardware-build-and-operation)
	2. [Software EDA build and operation](#software-eda-build-and-operation)
		1. [Layout](#layout)
		2. [Kicad Nightly](#kicad-nightly)
		3. [Where is the Kicad project?](#where-is-the-kicad-project)
		4. [3D Models and CAD](#3d-models-and-cad)
		5. [Pull Requests](#pull-requests)
	3. [Project Goals](#project-goals)
	4. [Status](#status)
		1. [TODO](#todo)

<!-- /TOC -->

## Hardware build and operation

A simple-as-possible implemenation of an oscillation with ≤ 0.03% THD, using
the variable resistance version of the HP200A AGC circuit.

It is built around a lamp, as the ideal PTC for the circuit, with particular
properties:

* 25mA current draw
* 12V
* ~50Ω resistance at 25°C
* Was sold under SKU 272-1441 from RS
* Stated purpose was train hobbyist light

Availability may be extremely difficult or very easy, depending on geography.
However, the Rb resistor is a wide, precision variable resistor, which can
be substituted if needed. However, by sheer luck, the 272-1441 is ideal.

![The lamp](docs/images/lamp-rs272-1141-small.png)

Frequency adjustment is accomplished with two ganged potentiometers, one
"fine" and one "coarse"; additionally, to cover 0-40KHz, a ganged switch is
also necessary to add resistors in a ladder.

## Software EDA build and operation

### Layout

```
.
├── docs
│   ├── images
│   └── models
│       └── scad
└── wien-oscillator-module
    └── wien-oscillator-module-backups
```

* `docs/` Everything and everything documentation, we like docs living with
  code for portability. Feel free to include added subdirectories here in PRs.
  Please check out Application Notes and Design Notes at `an.md` and
  `design.md`.
* `docs/images` Graphics; plots, previews, raytraces, etc. Always contains
  `latest-schematic.png`/`latest-schematic.pdf` for linking. `svg` would be
  preferable, and may return when a bug with Unicode characters is fixed.
* `docs/models` `stl`, `wrl`, etc. Anything that would be included in a custom
  component 3D package or footprint.
* `docs/models/*` "Source" for `models/` assets. Parameterizable is great,
  either the official FreeCAD pipeline, OpenSCAD, or something else. See
  [3D Models and CAD](#3d-models-and-cad).
* `wien-oscillator-module/` The KiCad project directory.

### Kicad Nightly

This project was started with and remains a Kicad "nightly" project; ie.,
`6.99`. Kicad's latest release at the time of writing (`6`) can run alongside
"KiCad-Nightly" in most distributions, and a separate installation will be
configured. This might be a little bit awkward for some, but Nightly is
already easier to use and has many must-have (in our opinion) features.

Installation of Nightly system-wide is not required to use it; however, it may
take additional disk space to have two copies of footprint libraries. In our
experience, though, besides the conversion of libraries, using Nightly is
painless.

### Where is the Kicad project?

Under `wien-oscillator-module/`, where `wien-oscillator-module.kicad_pro` is
located, is the KiCad project directory.

### 3D Models and CAD

3D models of components are located in `docs/models/`. This directory contains
the output `stl` and a preview bitmap, and the source files are under
subdirectories of their type; e.g., `scad/`.

While it's true that the KiCad mother project had picked FreeCAD as its
parameterized CAD tool, OpenSCAD also fits into this pipeline and is
parameterized (or models should be made to do so). There is a wealth of models
in many formats, and this seemed like the best solution to use all of the
compatibly-licensed models that are possible. In the end, the `STL` file and
the preview will be in `models/`.

Feel free to use your tool of choice, including FreeCAD and
`kicad-footprint-creator`. The only requirement is that "sources" live in a
subdirectory of `models/`, and that the output lives in `models/` itself with
a preview.

[Personally, though I have passed along models the official way, they always
originated in OpenSCAD and converted later to go through the FreeCAD process.
For one project, using OpenSCAD is perfectly fine, and converting if you wish
to contribute your 3D packages to KiCad is straightforward. As long as the
output of the pipeline can be used for the project, feel free to make a PR
however it was created.]

### Pull Requests

Feel free to send a PR in the usual way; or unusual way if circumstances
dictate that you need to send it via email or other means. Please check
"Issues" on GitLab to see if there is an appropriate ticket to name your
branch and first line of your Git commit, and feel free to create an issue
if it does not exist:

_In branch "9-awesome-improvement"_
```
9 Awesome Improvement to Widget

* Made awesome improvement A
* Made awesome improvement B
```

Vim (or NeoVim) will keep you on track with line length and such. "-m"s are
harder for us to cope with, but your PR won't _necessarily_ be rejected for
missing something from the above. Simple rule is just _nice commit messages_.
And being amenable to publication to a CHANGELOG.md via script is great (also
feel free to use Markdown in commit messages.)

## Project Goals

* Cheap as possible
* Low enough current draw for reliable operation with rechargeable battery,
  for safe use around possible ground loops in DUT and instrumentation.
* Easy to build, with board available on "shareable" systems; e.g., PCBWay
  (exists at the time of writing, 2022-12-3).
* Easy to find components, with only the PTC/lamp being an exception.
* OSHW, all analog design.
  * Not only OSHW and open-source, but using an all-libre toolchain; i.e.,
    KiCad, etc. and avoidance of incompatibly-licensed model generation. No
    Eagle, Altium, etc. Every design step is accessible on libre terms.

## Status

Boards mostly-complete, two-layer despite impedance matching needs.

Copper-clad prototypes work as desired, though extra functionality is
being planned:
* 50Ω connection to send original waveform to a scope, to enable a two-channel
  before and after.
* This would mean another gain stage to provide two outputs: one to inject
  into the DUT and one to send to the scope.
* As mention, switchable impedance matching for connecting to DUT via inputs
  as well as in-circuit.

Stackup: SS

### TODO
* Impedance matching networks. Would like to provide 50Ω, 75Ω, and high
  impedance internally with a switch, rather than make the user do this
  externally with dongles.
* Selection of high-availability pots, and creation of footprints and 3D
  models.

This is all that remains to complete!

_
People's Audio Analyzer Wave Gen
Copyright (C) 2022  Scott M. Drake <demonburrito@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
_
