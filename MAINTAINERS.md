# Maintainer Reference

## Purpose

Use this document for correspondence, in issues, or for a `cc:` field in your
commit, if you wish for your patch to receive the attention of a particular
maintainer.

It also serves as the canonical write-access list for the purposes of the
Developer Certificate of Origin.

## List of Maintainers by Specialty

### General
* Scott Drake <demonburrito@gmail.com> Initial code and general work.
